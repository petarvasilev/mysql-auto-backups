<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackupModel extends Model
{
    protected $table = 'backups';
    protected $fillable = ['file_name', 'path', 'database_name', 'deleted'];
}
