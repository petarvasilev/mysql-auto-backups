<?php

namespace App\Logic;

use App\BackupModel;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Backup {

    public function singleDatabaseCommand($databaseName, $fileName)
    {
        $fullPath = storage_path('app') . '/databases/' . $fileName;

        $username = config('backups.username');
        $password = config('backups.password');

        $cmd = "mysqldump --add-drop-table -u $username -p'$password' $databaseName > $fullPath";

        return $cmd;
    }

    public function allDatabasesCommand($fileName)
    {
        $fullPath = storage_path('app') . '/databases/' . $fileName;

        $username = config('backups.username');
        $password = config('backups.password');

        $cmd = "mysqldump -u $username -p'$password' --all-databases --skip-lock-tables > $fullPath";

        return $cmd;
    }

    public function generateFileName($databaseName)
    {
        $fileName = $databaseName . '.' . date('Y-m-d.H-i') . '.sql';

        return $fileName;
    }

    public function generateAllDatabasesFileName()
    {
        $fileName = 'all-databases' . '.' . date('Y-m-d.H-i') . '.sql';

        return $fileName;
    }

    public function individualDatabaseBackups()
    {
        $databases = config('backups.databases');

        foreach ($databases as $databaseName)
        {
            $fileName = $this->generateFileName($databaseName);
            $cmd = $this->singleDatabaseCommand($databaseName, $fileName);

            $process = new Process($cmd);
            $process->run(); // run backup command

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            Storage::disk('s3')->put(config('backups.s3-folder') . $fileName, Storage::get('databases/' . $fileName));

            if (config('backups.delete-local-copy'))
            {
                // delete the local copy the backup
                Storage::delete('databases/' . $fileName);
            }

            $backup = new BackupModel();
            $backup->file_name = $fileName;
            $backup->database_name = $databaseName;
            $backup->save();
        }
    }

    public function allDatabasesBackup()
    {
        $fileName = $this->generateAllDatabasesFileName();
        $command = $this->allDatabasesCommand($fileName);

        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        Storage::disk('s3')->put(config('backups.s3-folder') . $fileName, Storage::get('databases/' . $fileName));

        if (config('backups.delete-local-copy'))
        {
            // delete the local copy the backup
            Storage::delete('databases/' . $fileName);
        }

        $backup = new BackupModel();
        $backup->file_name = $fileName;
        $backup->database_name = 'all databases';
        $backup->save();
    }

    public function deleteOldBackups()
    {
        $timeAgo = time() - config('backups.delete-older-than') * 24 * 60 * 60;
        $datetime = date('Y-m-d H:i:s', $timeAgo);

        $backups = BackupModel::where('deleted', '=', 0)
            ->where('created_at', '<', $datetime)
            ->get();

        foreach ($backups as $backup)
        {
            if (Storage::exists('databases/' . $backup->file_name)) {
                Storage::delete('databases/' . $backup->file_name);
            }

            Storage::disk('s3')->delete(config('backups.s3-folder') . $backup->file_name);

            $backup->deleted = 1;
            $backup->save();
        }
    }

    public function make()
    {
        if (config('backups.all-databases'))
        {
            $this->allDatabasesBackup();
        }
        else
        {
            $this->individualDatabaseBackups();
        }
    }

}