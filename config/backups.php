<?php

return [

    /**
     * If this is set to true it will try to backup
     * all databases, if false it will backup only the
     * databases from 'databases'
     */
    'all-databases' => false,

    /**
     * The databases to backup if 'all-databases' is set to false
     */
    'databases' => [
        'database-name',
    ],

    /**
     * The times at which you want the backup to take place.
     * This must be in the format HH:MM
     */
    'times' => [
        '07:30',
    ],

    /**
     * The S3 folder in which to save the database backups
     */
    's3-folder' => 'databases/',

    /**
     * If true it will delete local copies of the backups once file has
     * been uploaded to S3
     */
    'delete-local-copy' => true,

    /**
     * Delete backups older than the specified amount of days
     */
    'delete-older-than' => 60,

    /**
     * the MySQL username & password that have access
     * to the databases you want to back up
     */
    'username' => env('DB_USERNAME'),
    'password' => env('DB_PASSWORD')

];