# MySQL Auto Backups

This a project which automatically backs up selected or all MySQL databases
to Amazon's S3. We use the 'mysqldump' command for that purpose.

This software is in Alpha so use at your own risk.

## Get started

Copy the project using git. Then do:

    composer install --no-dev
    
then do:

    chmod -R 777 storage
    
then fill in all the options in /config/backups.php as required.

then copy .env.example into .env and fill it in. Remember to fill in
DB_SOCKET as the lack of it could cause some issues.

run the migrations:

    php artisan migrate

then create a cron job in the following manner:

    * * * * * cd /path/to/project && php artisan schedule:run >> /dev/null 2>&1

now you are set to go.

## Options

You can see all available options and their descriptions in '/config/backups.php'.

## License

MIT License

Copyright 2020 [Petar Vasilev](https://www.petarvasilev.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.